# subst: perform string substitutions on files

subst is used to perform multiple string substitutions on multiple files. The substitution rules are read in from an appropriately-formatted file.

A subst file consists of zero or more **sections**, each of which consists of one or more **file lines** followed by one or more **rule lines**. Each rule is applied to each file.

Additionally, anywhere you could have a rule line, you can instead have an **antirule line**, which raises an error if it finds any matches.

Note that there is no explicit separator between sections: a new section starts whenever a file line follows a(n) (anti)rule line, for a reasonable value of "follows".

Finally, **comment lines** begin with a `#`. Inline comments are not supported, and non-comment lines may not contain `#` at all.

---

## Invocation and Syntax

subst is a shell script, so it should be ready to use out-of-the-box. Run it with `./subst.sh rules.subst`, where `rules.subst` is a file following the format documented below.

A file line follows the form:  
`:: ./path/to/input-file :: ./path/to/output-file`  
File paths **must** start with `./`. The same file(s) may appear in multiple sections. Including redundant file lines in a single section, and/or using file lines (even across sections) that imply a non-one-to-one mapping between input and output files, may yield surprising behavior and is not recommended.

A rule line follows the form:  
`>> VARIABLE >> Value`  
This causes the string `@@VARIABLE@@` to be replaced with the string `Value` in each file specified in the present section.  
Variable names may only contain uppercase A-Z and underscore (`_`). Note that when writing your input-file you must enclose the variable name in `@@`s, but when writing the rule line in your subst file you leave them off.

An antirule line follows the form:  
`!! VARIABLE`  
This raises an error if the string `@@VARIABLE@@` is found in any of the section's input-files.

---

## Example

If we have a file `rules.subst` containing the following:

```
:: ./input.txt :: output.txt

>> COLOR >> green
!! COLOUR

>> ANIMAL >> Elephant

```

and a file `input.txt` with the following contents:

```
My favorite color is @@COLOR@@.
MY FAVORITE ANIMAL IS THE @@ANIMAL@@.
I have never seen a @@COLOR@@ @@ANIMAL@@.
```

then running `./subst.sh rules.subst` should create (or overwrite) the file `output.txt` with the following contents:

```
My favorite color is green.
MY FAVORITE ANIMAL IS THE Elephant.
I have never seen a green Elephant.
```
