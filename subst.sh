#!/bin/bash

# Copyright (C) 2018  Sonata Green
# See LICENSE for more information.



if [ -z "$1" -o -n "$2" ]; then
    echo "Usage: $0 <name of file containing substitution rules>" >&2
    exit 1
fi


# Isn't there a better way to do this?
unset      _files_in_section _files_touched
declare -A _files_in_section _files_touched
declare _section_flag=

function _actualize_infile ()
{
    # Problem: suppose a given file is affected by multiple rules.
    # We want to apply each rule in series. This means that only
    # the first substitution can use the source infile -- otherwise,
    # later rules would clobber previous changes. But for the first
    # rule, we want to make sure we *do* clobber any previous version.
    #   Solution: keep a list of files touched, so we know whether any
    # given rule is the first one we're applying to any given file.
    #   Using an associative array for _files_touched makes it
    # slightly more convenient to check whether it contains a given
    # element (key), and to avoid unnecessary duplicates.

    if [ -n "${_files_touched[$_outfile]}" ]; then
        _infile_actual="$_outfile"
    else
        _infile_actual="$_srcfile"
    fi
}

# I know that unnecessary use of cat is considered Bad Style.
# I disagree in this case: related code should be near each other.
# I'm not moving half a pipeline to the bottom of the loop just to
# save one process call. If you don't like it, fix the language.
cat "$1" | while read _line ||
        [[ -n "$_line" ]]
do
    case "$_line" in
        ### Comments ###
        # Lines starting with '#' are comments.
        #   As a matter of good style, it's recommended to avoid starting
        # a line with exactly two #s, to avoid visual similarity to
        # noncomment line prefixes (:: >> !!).
        \#*) ;;
        '') ;; # Blank lines are likewise considered no-ops.
        *#*)
            # If a non-comment line contains a '#', the user likely
            # made a mistake (subst doesn't support inline comments).
            # This does prevent using '#' in rules, but oh well.
            #if [ "$(echo $_line | head -c 1)" != "#" ]; then
                echo "Error: '#' character found outside comment line." >&2
                echo "$_line" >&2
                exit 2
            #fi
            ;;
        
        ### File Lines ###
        # :: ./path/to/input-file :: ./path/to/output-file
        ':: '*)
            if [ -n "$_section_flag" ]; then
                # A group of file lines applies to a group of rule lines
                # (and/or antirule lines).
                #   The file lines precede the rule lines to which they
                # apply. A file line after a(n) (anti)rule line starts
                # a new section.
                unset      _files_in_section
                declare -A _files_in_section
                _section_flag=
            fi
            
            declare _infile="$(
                echo "$_line" |
                tail -c +4 - |
                sed -e 's/ :: .*$//' -
            )"
            declare _outfile="$(
                echo "$_line" |
                sed -e 's/^:: .* :: //' -
            )"
            # As syntactic salt, file names must begin with ./
            if [ "$(echo "$_infile"  | head -c 2 -)" != "./"  -o \
                 "$(echo "$_outfile" | head -c 2 -)" != "./" \
               ]; then
                echo "Error: files must start with ./" >&2
                echo "  $_line" >&2
                exit 3
            fi
            
            _files_in_section["$_infile"]="$_outfile"
            ;;
        
        ### Rule and Antirule Lines ###
        # >> VARIABLE >> Value
        # !! VARIABLE
        # Rule lines cause '@@VARIABLE@@' to be substituted with 'Value'.
        # Antirule lines cause '@@VARIABLE@@' to generate an error.
        #   Note that when writing an input file, you use @@VARIABLE@@,
        # but when writing a(n) (anti)rule line, you leave off the @s.
        '>> '*) # rule line
            _section_flag='1'
            
            if echo "$_line" | egrep -vc '^>> [A-Z_]+ >> ' >/dev/null; then
                echo "Error: malformed rule line. Variable names may only include uppercase A-Z and underscore (_)." >&2
                exit 4
            fi
            
            # Code duplication. :(
            # I could factor it out, but I'm being lazy.
            declare _varname="$(
                echo "$_line" |
                tail -c +4 - |
                sed -re 's/ >> .*$//' -
            )"
            declare _varvalue="$(
                echo "$_line" |
                tail -c +4 - |
                sed -re 's/^[A-Z_]+ >> //' -
            )"
            
            declare _srcfile
            [ -z "${_files_in_section[*]}" ] ||
                for _srcfile in ${!_files_in_section[@]}; do
                    declare _outfile="${_files_in_section[$_srcfile]}"
                    declare _infile_actual
                    _actualize_infile
                    _files_touched["$_outfile"]=1
                    
                    # It's not "technically" necessary to use a buffer file
                    # when _infile_actual==_srcfile, but (I speculate) it
                    # might protect against leaving a partially-written
                    # outfile if the process is interrupted.
                    declare _bufferfile="/tmp/subst_$BASHPID_$RANDOM"
                    (
                        flock 3
                        sed -e "s/@@${_varname}@@/${_varvalue}/g" \
                            "$_infile_actual" > $_bufferfile
                        mv -f $_bufferfile "$_outfile"
                    ) 3>$_bufferfile
                done
            ;;
        
        '!! '*) # antirule line
            _section_flag='1'
            # More duplicated code. :(
            if echo "$_line" | egrep -xvc '!! [A-Z_]+' >/dev/null; then
                echo "Error: malformed antirule line. Variable names may only include uppercase A-Z and underscore (_)." >&2
                exit 5
            fi
            [ -z "${_files_in_section[*]}" ] ||
                for _srcfile in ${!_files_in_section[@]}; do
                    declare _varname="$(
                    echo "$_line" |
                    tail -c +4 -
                )"
                    declare _outfile="${_files_in_section[$_srcfile]}"
                    declare _infile_actual
                    _actualize_infile
                    
                    grep "@@${_varname}@@" "$_infile_actual" >/dev/null &&
                        echo "Error: antirule variable @@${_varname}@@ found while processing file ${_srcfile}" >&2 &&
                        exit 6
                done
            ;;

        *)
            echo "Unrecognized line type:" >&2
            echo "$_line" >&2
            exit 7
            ;;
    esac
done
exit 0	# Why is this necessary?
